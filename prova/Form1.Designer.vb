﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.boton_inicial = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.moviment = New System.Windows.Forms.PictureBox()
        CType(Me.moviment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'boton_inicial
        '
        Me.boton_inicial.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.boton_inicial.Location = New System.Drawing.Point(22, 25)
        Me.boton_inicial.Name = "boton_inicial"
        Me.boton_inicial.Size = New System.Drawing.Size(108, 63)
        Me.boton_inicial.TabIndex = 0
        Me.boton_inicial.Text = "Button1"
        Me.boton_inicial.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(218, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "magdalena magdalena"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(222, 89)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 26)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Text = "magdalenilla"
        '
        'moviment
        '
        Me.moviment.Image = Global.prova.My.Resources.Resources._22_TABLERO_POPULAR_50_CM
        Me.moviment.InitialImage = CType(resources.GetObject("moviment.InitialImage"), System.Drawing.Image)
        Me.moviment.Location = New System.Drawing.Point(499, 107)
        Me.moviment.Name = "moviment"
        Me.moviment.Size = New System.Drawing.Size(175, 158)
        Me.moviment.TabIndex = 3
        Me.moviment.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.moviment)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.boton_inicial)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.moviment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents boton_inicial As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents moviment As PictureBox
End Class
